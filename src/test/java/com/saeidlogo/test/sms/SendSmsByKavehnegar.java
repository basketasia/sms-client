/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.test.sms;

import com.basketasia.database.service.Application;
import com.basketasia.database.service.DatabaseService;
import com.saeidlogo.sms.client.SmsService;
import com.saeidlogo.sms.client.SmsServiceInterface;
import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 *
 */
@RunWith(Arquillian.class)
public class SendSmsByKavehnegar {

    @Deployment
    public static WebArchive deployService() {
        return ShrinkWrap.create(WebArchive.class)
                .addClasses(SmsService.class, DatabaseService.class,Application.class);
    }

    @EJB
    private SmsServiceInterface service;

    @Test
    public void sendSmsNormalKavehnegar() throws Exception {
        service.send("salam", "+989126586031");
    }
}
