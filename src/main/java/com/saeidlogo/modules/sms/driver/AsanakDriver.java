/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.modules.sms.driver;

import com.basketasia.database.service.DatabaseServiceLocal;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.saeidlogo.sms.client.SMSException;
import com.saeidlogo.sms.client.SmsResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author saeidlogo
 */
public class AsanakDriver extends BaseSmsDriver implements SMSDriverInterface {

    private Log logger = LogFactory.getLog(AsanakDriver.class);

    // Module name must be the same as the class name although it can be all in lowercase
    private static String code = "asanak";

    private static String mutex = "asanakMutex";

    // Configuration Keys
    private final static String MODULE_ASANAK_BASE_URL = "MODULE_ASANAK_BASE_URL";
    private final static String MODULE_ASANAK_USERNAME = "MODULE_ASANAK_USERNAME";
    private final static String MODULE_ASANAK_PASSWORD = "MODULE_ASANAK_PASSWORD";
    private final static String MODULE_ASANAK_SENDER = "MODULE_ASANAK_SENDER";
    private final static String MODULE_ASANAK_STATUS = "MODULE_ASANAK_STATUS";

    public static String SEND_SMS = "/sendsms";

    public static String MESSAGE_STATUS = "/msgstatus";

    public static int NOT_FOUND = -1;
    public static int NOT_IN_QUEUE = 0;
    public static int IN_QUEUE = 1;
    public static int PENDING = 4;
    public static int SENT = 2;
    public static int NO_RESPONSE = 7;
    public static int REJECT = 8;
    public static int PARTIALY_SENT = 10;
    public static int SENT_TO_DEST = 9;
    public static int SUCCESS = 6;
    public static int FAILED = 5;
    public static int NO_MSG_ID = 11;
    public static int PARTIALLY_SUCCESS = 12;
    public static int NO_DELIVERY = 13;

    /**
     * Hash Map that contains the static data
     */
    private static Map<String, StaticData> staticDataHM = Collections
            .synchronizedMap(new HashMap<String, StaticData>());

    /**
     * Constructor
     *
     * @param sessionId
     * @param eng
     *
     * @throws KKException
     */
    public AsanakDriver(DatabaseServiceLocal databaseService) throws Exception {
        super(databaseService);
        StaticData sd = staticDataHM.get(getStoreId());
        if (sd == null) {
            synchronized (mutex) {
                sd = staticDataHM.get(getStoreId());
                if (sd == null) {
                    setStaticVariables();
                }
            }
        }
    }

    @Override
    public SmsResult sendRequest(String message, String destination) throws SMSException {
        SmsResult status = new SmsResult();
        String content = message.trim();
        StaticData sd = staticDataHM.get(getStoreId());
        try {
            HttpResponse<String> response = Unirest.get(sd.getBaseUrl() + SEND_SMS)
                    .header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
                    .header("accept", "text/html")
                    .header("Connection", "Keep-Alive")
                    .queryString("username", sd.getUsername())
                    .queryString("password", sd.getPassword())
                    .queryString("source", sd.getSender())
                    .queryString("destination", destination)
                    .queryString("message", content)
                    .asString();
            if (response.getStatus() == 200) {
                if (response.getStatusText() != null && response.getStatusText().equalsIgnoreCase("OK")) {
                    logger.info(message + " sent successfuly");
                } else {
                    logger.error(message + " was not sent successfuly");
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return status;
    }

    @Override
    public boolean isAvailable() throws Exception {
        return getConfigurationValueAsBool(MODULE_ASANAK_STATUS, false);
    }

    @Override
    public void setStaticVariables() throws Exception {
        String value;
        StaticData staticData = staticDataHM.get(getStoreId());
        if (staticData == null) {
            staticData = new StaticData();
            staticDataHM.put(getStoreId(), staticData);
        }
        

        value = getConfigurationValue(MODULE_ASANAK_BASE_URL);
        if (value == null) {
            throw new Exception(
                    "The Configuration MODULE_ASANAK_BASE_URL failed.");
        }
        staticData.setBaseUrl(value);

        value = getConfigurationValue(MODULE_ASANAK_SENDER);
        if (value == null) {
            throw new Exception(
                    "The Configuration MODULE_ASANAK_SENDER failed.");
        }
        staticData.setSender(value);

        value = getConfigurationValue(MODULE_ASANAK_USERNAME);
        if (value == null) {
            throw new Exception(
                    "The Configuration MODULE_ASANAK_USERNAME failed.");
        }
        staticData.setUsername(value);

        value = getConfigurationValue(MODULE_ASANAK_PASSWORD);
        if (value == null) {
            throw new Exception(
                    "The Configuration MODULE_ASANAK_PASSWORD failed.");
        }
        staticData.setPassword(value);

    }

    @Override
    public String getDriverCode() {
        return code;
    }

    /**
     * Used to store the static data of this module
     */
    @Setter
    @Getter
    protected class StaticData {

        private int sortOrder = -1;
        private String username;
        private String password;
        private String baseUrl;
        private String sender;

    }
}
