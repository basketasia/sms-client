/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.modules.sms.driver;

import com.basketasia.database.service.DatabaseServiceLocal;

/**
 *
 *
 */
public class BaseSmsDriver {

    private final DatabaseServiceLocal databaseService;

    public BaseSmsDriver(DatabaseServiceLocal databaseService) {
        this.databaseService = databaseService;
    }

    public String getStoreId() {
        return "store1";
    }

    public String getConfigurationValue(String key) throws Exception {
        return databaseService.getConfigurationValue(key);
    }

    protected boolean getConfigurationValueAsBool(String key, boolean def)
            throws Exception {
        return Boolean.parseBoolean(getConfigurationValue(key));
    }
}
