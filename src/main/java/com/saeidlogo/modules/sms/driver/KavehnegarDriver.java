/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.modules.sms.driver;

import com.basketasia.database.service.DatabaseServiceLocal;
import com.kavenegar.sdk.KavenegarApi;
import com.kavenegar.sdk.models.SendResult;
import com.saeidlogo.sms.client.SMSException;
import com.saeidlogo.sms.client.SmsResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author saeidlogo
 */
public class KavehnegarDriver extends BaseSmsDriver implements SMSDriverInterface {

    // Module name must be the same as the class name although it can be all in lowercase
    private static String code = "kavehnegar";

    private static String mutex = "kavehnegarMutex";

    // Configuration Keys
    private final static String MODULE_KAVEHNEGAR_API_TOKEN = "MODULE_KAVEHNEGAR_API_TOKEN";
    private final static String MODULE_KAVEHNEGAR_SENDER = "MODULE_KAVEHNEGAR_SENDER";
    private final static String MODULE_KAVEHNEGAR_STATUS = "MODULE_KAVEHNEGAR_STATUS";

    /**
     * Hash Map that contains the static data
     */
    private static Map<String, StaticData> staticDataHM = Collections
            .synchronizedMap(new HashMap<String, StaticData>());

    /**
     * Constructor
     *
     * @param sessionId
     * @param eng
     *
     * @throws KKException
     */
    public KavehnegarDriver(DatabaseServiceLocal databaseService) throws Exception {
        super(databaseService);
        StaticData sd = staticDataHM.get(getStoreId());
        if (sd == null) {
            synchronized (mutex) {
                sd = staticDataHM.get(getStoreId());
                if (sd == null) {
                    setStaticVariables();
                }
            }
        }
    }

    @Override
    public SmsResult sendRequest(String message, String destination) throws SMSException {
        SmsResult result = new SmsResult();
        StaticData sd = staticDataHM.get(getStoreId());
        try {
            KavenegarApi api = new KavenegarApi(sd.getApiKey());
            SendResult rs = api.send(sd.getSender(), destination, message);
            result.setStatus(rs.getStatus());
            if (rs.getStatus() == 200) {
                result.setMessage(rs.getMessage());
                result.setCost(String.valueOf(rs.getCost()));
                result.setMessageId(rs.getMessageId());
            }
        } catch (Exception e) {
            throw new SMSException(e);
        }
        return result;
    }

    @Override
    public boolean isAvailable() throws Exception {
        boolean available = getConfigurationValueAsBool(MODULE_KAVEHNEGAR_STATUS, false);
        return available;
    }

    @Override
    public void setStaticVariables() throws Exception {
        StaticData staticData = staticDataHM.get(getStoreId());
        if (staticData == null) {
            staticData = new StaticData();
            staticDataHM.put(getStoreId(), staticData);
        }

        String apiKey = getConfigurationValue(MODULE_KAVEHNEGAR_API_TOKEN);
        staticData.setApiKey(apiKey);

        String sender = getConfigurationValue(MODULE_KAVEHNEGAR_SENDER);
        staticData.setSender(sender);

    }

    @Override
    public String getDriverCode() {
        return code;
    }

    /**
     * Used to store the static data of this module
     */
    @Setter
    @Getter
    protected class StaticData {

        private int sortOrder = -1;

        private String apiKey;
        private String sender;
        private boolean tplBase;
        private String template;

    }

}
