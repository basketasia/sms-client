/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.modules.sms.driver;

import com.saeidlogo.sms.client.SMSException;
import com.saeidlogo.sms.client.SmsResult;

/**
 *
 * @author saeidlogo
 */
public interface SMSDriverInterface {

    public SmsResult sendRequest(String message, String destination) throws SMSException;

    public boolean isAvailable() throws Exception;

    public void setStaticVariables() throws Exception;

    public String getDriverCode();

}
