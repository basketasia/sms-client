/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.sms.client;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author saeidlogo
 */
@Setter
@Getter
public class Message {

    private String MsgID;
    private String Status;
    private String SendTime;
    private String DeliverTime;
}
