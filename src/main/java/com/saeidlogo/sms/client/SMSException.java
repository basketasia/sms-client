/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.sms.client;


/**
 *
 * @author saeidlogo
 */
public class SMSException extends Exception {

    public SMSException() {
    }

    public SMSException(String message) {
        super(message);
    }

    public SMSException(Throwable ex) {
        super(ex);
    }
}
