/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.sms.client;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author saeidlogo
 */
@Setter
@Getter
public class SmsResult {

    private long messageId;
    private String network;
    private String cost;

    public static final int SMS_ERROR_INVALID_PARAMETERS = 414;
    public static final int SMS_ERROR_INVALID_DATE = 417;
    public static final int SMS_ERROR_NOT_ENOUGH_CREADIT = 418;

    private String message;
    private int status;
    private String smsText;
    private String statustext;
    private String sender;
    private String receptor;
    private long date;
    private int type;

    private long clientId;
}
