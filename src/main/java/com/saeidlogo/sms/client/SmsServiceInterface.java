/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.sms.client;

import com.saeidlogo.modules.sms.driver.SMSDriverInterface;
import javax.ejb.Local;

/**
 *
 * @author saeidlogo
 */
@Local
public interface SmsServiceInterface {

    public SmsResult send(String message, String destination) throws SMSException;

    public SmsResult send(String message, String destination, String preferred) throws SMSException;

    public void refreshConfigs() throws Exception;

    public SMSDriverInterface[] getSMSDrivers();

    public boolean isReady();
}
