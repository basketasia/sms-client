/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.sms.client;

import com.basketasia.database.service.DatabaseServiceLocal;
import com.saeidlogo.modules.sms.driver.SMSDriverInterface;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;

/**
 *
 * @author saeidlogo
 */
@Singleton
public class SmsService implements SmsServiceInterface {

    // Configuration Keys
    protected static final String MODULE_SMS_INSTALLED = "MODULE_SMS_INSTALLED";

    public static final String BASE_PACKAGE = "com.saeidlogo.modules";
    protected String storeId = "store1";

    private boolean ready;

    @EJB(name = "databaseService")
    private DatabaseServiceLocal databaseService;

    // Configuration data
    private List<String> smsModuleList;

    private List<SMSDriverInterface> smsDrivers;

    @Override
    public SmsResult send(String message, String destination) throws SMSException {
        return send(message, destination, null);
    }

    @Override
    public SmsResult send(String message, String destination, String preferred) throws SMSException {
        SMSDriverInterface[] drivers = getSMSDrivers();
        if (drivers == null || drivers.length == 0) {
            throw new SMSException("No driver is installed");
        }

        String ccode = preferred == null ? findCodeFromDest(destination) : preferred;

        //Try to determine correct driver from preferred and destination options
        SMSDriverInterface selectedDriver = null;
        try {
            for (SMSDriverInterface driver : drivers) {
                if (driver.isAvailable()) {
                    String code = driver.getDriverCode();
                    if (ccode.equalsIgnoreCase(code)) {
                        selectedDriver = driver;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            throw new SMSException(e);
        }

        if (selectedDriver == null) {
            throw new SMSException("No driver was found");
        }

        return selectedDriver.sendRequest(message, destination);
    }

    @Override
    public void refreshConfigs() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isReady() {
        return ready;
    }

    @PostConstruct
    public void init() {
        //initialize kkserviecprovider with sessionId
        //Configuration SMS Asanak Service
//            String smsBaseUrl = getEng().getConfigurationByKey(sessionId, "SMS_ASANAK_BASE_URL").getConfigurationValue();
//            String smsUserName = getEng().getConfigurationByKey(sessionId, "SMS_ASANAK_USERNAME").getConfigurationValue();
//            String smsPassword = getEng().getConfigurationByKey(sessionId, "SMS_ASANAK_PASSWORD").getConfigurationValue();
//            String smsSource = getEng().getConfigurationByKey(sessionId, "SMS_ASANAK_SENDER").getConfigurationValue();
        smsDrivers = new ArrayList();
        smsModuleList = new ArrayList();
        try {
            String modulesString = databaseService.getConfigurationValue(MODULE_SMS_INSTALLED);
            if (modulesString != null) {
                String[] modulesStringArray = modulesString.split(";");
                for (String mname : modulesStringArray) {
                    // Remove any extension that the file name may have such as php
                    String[] moduleNameExtArray = mname.split("\\.");
                    smsModuleList.add(getJavaModuleName("sms.driver", moduleNameExtArray[0]));
                }
            }

            // Now we need to get a list of Payment modules to refresh them all
            for (String moduleName : smsModuleList) {
                // Instantiate the module
                if (moduleName != null) {
                    try {
                        SMSDriverInterface smsModule = getModuleForName(moduleName);
                        smsModule.setStaticVariables();
                        smsDrivers.add(smsModule);
                    } catch (Throwable e) {
                        System.out.println("Could not instantiate the SMS Module " + moduleName
                                + " in order to refresh its configuration.");
                    }
                }
            }
            ready = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getJavaModuleName(String prefix, String moduleName) {
        if (moduleName == null || moduleName.length() == 0) {
            return null;
        }
        String baseName = BASE_PACKAGE + "." + prefix + ".";
        String s1 = moduleName.substring(0, 1);
        String s2 = moduleName.substring(1, moduleName.length());
        String retName = baseName + s1.toUpperCase() + s2;
        return retName;
    }

    private SMSDriverInterface getModuleForName(String moduleName)
            throws IllegalArgumentException, InstantiationException, IllegalAccessException,
            InvocationTargetException, ClassNotFoundException, Exception {
        Class<?> moduleClasses = Class.forName(moduleName);
        SMSDriverInterface module;
        Constructor<?>[] constructors = moduleClasses.getConstructors();
        Constructor<?> engConstructor = null;
        if (constructors != null && constructors.length > 0) {
            for (Constructor<?> constructor : constructors) {
                Class<?>[] parmTypes = constructor.getParameterTypes();
                if (parmTypes != null && parmTypes.length == 1) {
                    engConstructor = constructor;
                }
            }
        }

        if (engConstructor != null) {
            module = (SMSDriverInterface) engConstructor.newInstance(databaseService);
            return module;
        }
        return null;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
    public SMSDriverInterface[] getSMSDrivers() {
        return smsDrivers.toArray(new SMSDriverInterface[smsDrivers.size()]);
    }

    //ToDo
    //Should be implemented in tables
    private String findCodeFromDest(String destination) {
        if (destination.startsWith("+98")) {
            return "kavehnegar";
        }
        return "nexmo";
    }
}
